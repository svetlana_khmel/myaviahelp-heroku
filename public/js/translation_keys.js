var $ = jQuery = require('jquery');
//var i18n = require('./lib/i18n');

(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.translate = factory();
    }
}(this, function ($) {
    var language = window.navigator.userLanguage || window.navigator.language;
    var local = language.split("-")[0];
    var switcher = document.querySelectorAll('.switch-lang');
    var data = {};
    var currentLang;

    if(localStorage.getItem('currentLang')) {
        currentLang = localStorage.getItem('currentLang');
    } else {
        currentLang = local;
        localStorage.setItem('currentLang', local);
    }

    for (var i = 0; i < switcher.length; i++) {
        var local = switcher[i].getAttribute('data-switch');
        switcher[i].addEventListener('click', switchLanguage);
    }

    loadData(currentLang);

    function loadData(local, cb) {
        var storedData = localStorage.getItem(local);

        if (storedData) {
            data = storedData;
            if (typeof cb === 'function') {
                cb();
            }
        } else {
            var xhr = new XMLHttpRequest();
            /// xhr.open('get', '../../../lang/'+ local +'.json', true);
            xhr.open('get', '/lang/'+ local, true);

            xhr.onreadystatechange = function () {
                if (xhr.readyState == XMLHttpRequest.DONE && xhr.status == 200) {
                    //console.log(xhr.responseText);
                    console.log("resp...", xhr);

                    data = xhr.responseText;
                    storeData(local, data);
                }
            };

            xhr.send();
        }
    }

    function storeData(local, data) {
        if (typeof(Storage) !== "undefined") {
            localStorage.setItem(local, data);
            loaded();
        } else {
            // Sorry! No Web Storage support..
        }
    }

    function storeCurrentLanguage(lang, cb) {
        currentLang = lang;
        if(typeof (Storage) != "undefined") {
            localStorage.setItem('currentLang', currentLang);
            loadData(currentLang, cb);

        } else {
            // Sorry! No Web Storage support..
        }
    }

    function switchLanguage(e) {
        e.preventDefault();

        var lang = this.getAttribute('data-switch');

        // var url = window.location.href;
        // if (url.indexOf('?') > -1){
        //     url += '?lang='+ lang;
        // }else{
        //     url += '?lang='+ lang;
        // }
        // window.location.href = url;
        storeCurrentLanguage(lang, loaded);
    }

    String.prototype.toLocaleString = function () {
        var string =  this.valueOf();
        var translations = localStorage.getItem(currentLang);
        var obj = JSON.parse(translations);
        return obj[string];
    };

    function loaded (modal) {

        localizeHTMLTag(modal);
    }

    /* Some helper functions: */

    var _ = function (string) {
        return string.toLocaleString();
    };

    function localizeHTMLTag(modal) {
        if (modal) {
            var elm = modal.querySelectorAll("[data-i18n']");
        } else {
            var elm = document.querySelectorAll("[data-i18n]");
        }

        elm.forEach(function(val, index) {
            val.innerHTML = _(val.getAttribute('data-i18n'));
        });
    }

    function translate (modal) {
        loaded(modal);
    }

    return translate;
    /* End localization: */
}));
