# What has been used.

Nodejs, Express, Mongodb(Mongoose), Handlebars, Bootstrap 3, jQuery, i18n like, sw-precache to off-line work, manifest.xml to mobile, Grunt to build, browserify to support Commonjs.


*** Own implementation i18n lib
*** Mongodb to store data
*** Handlebars as template engine
*** serviceWorker for working offline
*** Bootstrap 3 elements (Navigation, Scrollspy, Carouser)
*** Site is responsive
*** Commonjs like modules.





# node-js-getting-started

Site available by link https://secure-earth-56975.herokuapp.com/

A barebones Node.js app using [Express 4](http://expressjs.com/).

This application supports the [Getting Started with Node on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs) article - check it out.

## Running Locally

Make sure you have [Node.js](http://nodejs.org/) and the [Heroku CLI](https://cli.heroku.com/) installed.

```sh
$ git clone git@github.com:heroku/node-js-getting-started.git # or clone your own fork
$ cd node-js-getting-started
$ npm install
$ npm start
```

Your app should now be running on [localhost:5000](http://localhost:5000/).

## Deploying to Heroku

```
$ heroku create
$ git push heroku master
$ heroku open
```
or

[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## Documentation

For more information about using Node.js on Heroku, see these Dev Center articles:

- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/getting-started-with-nodejs)
- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Node.js on Heroku](https://devcenter.heroku.com/categories/nodejs)
- [Best Practices for Node.js Development](https://devcenter.heroku.com/articles/node-best-practices)
- [Using WebSockets on Heroku with Node.js](https://devcenter.heroku.com/articles/node-websockets)



*** Helpfull articles: ***

https://www.tutorialspoint.com/mongodb/mongodb_query_document.htm



db.createUser(
      {
        user: "myUserAdmin",
        pwd: "abc123",
        roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
      }
    )


    show users



mongo --port 27017 -u "myUserAdmin" -p "abc123" --authenticationDatabase "admin"