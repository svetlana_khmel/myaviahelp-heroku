(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.Rellax = factory();
    }
}(this, function () {

    var rules = [
        {
            "component": "input",
            "name": "username",
            "type": "text",
            "pattern": "^[a-zA-Z0-9]+([a-zA-Z0-9](_|-| )[a-zA-Z0-9])*[a-zA-Z0-9]+$",
            "messages": {
                "required": "This field can`t be empty",
                "positive": "",
                "negative": "Usernames can consist of lowercase and capitals. Usernames can consist of alphanumeric characters. Usernames can consist of underscore and hyphens and spaces. Cannot be two underscores, two hypens or two spaces in a row. Cannot have a underscore, hypen or space at the start or end."
            },
            "min": 4,
            "max": 40,
            "required": true,
            "label": "User name"
        },
        {
            "component": "input",
            "name": "email",
            "type": "email",
            "pattern": "%5CS+@%5CS+%5C.%5CS+",
            "messages": {
                "required": "This field can`t be empty",
                "positive": "",
                "negative": "Don`t forget @."
            },
            "min": 4,
            "max": 40,
            "label": "Email"
        },
        {
            "component": "input",
            "name": "phone",
            "type": "text",
            "pattern": "^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-%2Fs%2F./0-9]*$",
            "messages": {
                "required": "This field can`t be empty",
                "positive": "",
                "negative": "Format is not correct."
            },
            "min": 4,
            "max": 40,
            "required": true,
            "label": "Phone"
        }
    ];

    function validateForm (data, dfd) {
        var validatedFields = {};

        data.forEach(function (el, index) {

            if (el.name === 'username') {

                rules.forEach(function (obj, index) {

                    if (obj.name === 'username') {

                        var dataToCheck = el.value;
                        var pattern = new RegExp(decodeURIComponent(obj.pattern));

                        if (pattern.test(dataToCheck) === true) {
                            validatedFields.username = 'passed';
                        } else {
                            validatedFields.username = obj.messages.negative;
                        }

                        if(dataToCheck === '') {
                            validatedFields.username = obj.messages.required;
                        }
                    }
                });
            }

            if (el.name === 'email') {

                rules.forEach(function (obj, index) {

                    if (obj.name === 'email') {

                        var dataToCheck = el.value;
                        var pattern = new RegExp(decodeURIComponent(obj.pattern));

                        if (pattern.test(dataToCheck) === true) {
                            validatedFields.email = 'passed';
                        } else {
                            validatedFields.email = obj.messages.negative;
                        }

                        if(dataToCheck === '') {
                            validatedFields.email = obj.messages.required;
                        }
                    }
                });
            }

            if (el.name === 'phone') {

                rules.forEach(function (obj, index) {

                    if (obj.name === 'phone') {

                        var dataToCheck = el.value;
                        var pattern = new RegExp(decodeURIComponent(obj.pattern));

                        if (pattern.test(dataToCheck) === true) {
                            validatedFields.phone = 'passed';
                        } else {
                            validatedFields.phone = obj.messages.negative;
                        }

                        if(dataToCheck === '') {
                            validatedFields.phone = obj.messages.required;
                        }
                    }
                });
            }

            dfd.resolve(validatedFields);
        });
    }

    var Validation = function(data, dfd) {
        validateForm(data, dfd);
    };

    return Validation;

}));

